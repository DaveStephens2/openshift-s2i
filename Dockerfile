FROM registry.access.redhat.com/openjdk/openjdk-11-rhel7:1.1

# Builder version
ENV BUILDER_VERSION 1.0

LABEL io.k8s.description="Platform for building Spring Boot applications with gradlew" \
      io.k8s.display-name="Spring Boot builder 1.0" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="Java,Springboot,builder"

# Need to move back up to root from the jboss user the base image runs as, move back down to jboss later
USER root

# Make the required dierctories 
RUN mkdir -p /opt/openshift

# Change perms on target/deploy directory to 777
RUN chmod -R 777 /opt/openshift #/opt/app-root/src

# Copy the S2I scripts. The location of theses scripts is set in the base image 
# by it setting the 'io.openshift.s2i.scripts-url' label. check labels with 
# 'docker inspect <BASE_IMAGE>'
COPY ./s2i/bin/ /usr/local/s2i/
RUN chown -R jboss:root /usr/local/s2i && chmod 777 /usr/local/s2i/*

# Set the default port for applications built using this image
EXPOSE 8080

# This default user is created in the openshift/base-centos7 image (jboss)
USER 185

# Set the default CMD for the image
CMD ["/usr/local/s2i/usage"]